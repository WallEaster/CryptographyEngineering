import hashlib

mPrefix = "CO409CryptographyEngineeringRunsNowForItsThirdYear"
nonce = 0
target = "000000"
m = mPrefix + str(nonce)
h = ""

while not (h.startswith(target)):
    m = mPrefix + str(nonce)
    d = hashlib.sha256(m)
    d2 = hashlib.sha256(d.digest())
    h = d2.hexdigest()
    nonce += 1

print m, h
